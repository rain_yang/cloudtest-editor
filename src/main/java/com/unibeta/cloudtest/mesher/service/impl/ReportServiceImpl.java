package com.unibeta.cloudtest.mesher.service.impl;

import java.io.File;

import org.springframework.stereotype.Service;

import com.unibeta.cloudtest.mesher.config.ConfigurationProxy;
import com.unibeta.cloudtest.mesher.dto.Option;
import com.unibeta.cloudtest.mesher.dto.Report;
import com.unibeta.cloudtest.mesher.service.ReportService;

@Service
public class ReportServiceImpl implements ReportService {

	@Override
	public Report getReport(String username) {
		Report report = new Report();

		report.setReportBaseUrl(ConfigurationProxy.getAPIHost() + "/workspace/" + username + "/reports/");

		File reports = new File(ConfigurationProxy.getCLOUDTEST_HOME() + "/reports");
		if (reports.exists()) {

			String files[] = reports.list();

			for (String s : files) {
				Option option = new Option();
				option.setLabel(s);
				option.setValue(s);
				report.getReportList().add(option);
			}

			return report;
		} else {
			return report;
		}
	}

}
