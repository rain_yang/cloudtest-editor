package com.unibeta.cloudtest.mesher.service;

import com.unibeta.cloudtest.mesher.dto.Report;

public interface ReportService {

	public Report getReport(String username);
}
