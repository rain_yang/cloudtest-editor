package com.unibeta.cloudtest.mesher.service;

import java.util.List;

import com.unibeta.cloudtest.mesher.modules.upm.entity.Tenant;
import com.unibeta.cloudtest.mesher.modules.upm.entity.User;

public interface UpmService {
	
	public User registerUser(User user) throws Exception;
	public boolean updateUser(User user) throws Exception;
	public User login(User user) throws Exception;
	public User getUserByName(String name) throws Exception;
	public User getUserById(Long id) throws Exception;
	public List<Tenant> getTenantList(Long userId) throws Exception;
	public Tenant addTenant(Tenant tenant) throws Exception;
	public Boolean changePassword(String username, String oldPassword, String newPassword, String confirmPassword) throws Exception;
}
