package com.unibeta.cloudtest.mesher.service;

import java.util.List;

import com.unibeta.cloudtest.mesher.modules.schedule.entity.Job;
import com.unibeta.cloudtest.mesher.modules.schedule.entity.JobRunHistory;

public interface ScheduleService {

	List<Job> getJobsByUserId(Long userId) throws Exception;

	Job getJob(Long jobId) throws Exception;

	Job createJob(Job job) throws Exception;

	void removeJob(Long jobId) throws Exception;

	Job updateJob(Job job) throws Exception;

	Boolean startJob(Long jobId) throws Exception;

	Boolean stopJob(Long jobId) throws Exception;
	
	JobRunHistory saveJobRunHistory(JobRunHistory jobRunHistory) throws Exception;

	List<JobRunHistory> getJobRunHistoryList(Long jobId) throws Exception;
}
