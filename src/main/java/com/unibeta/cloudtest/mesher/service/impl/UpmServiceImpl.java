package com.unibeta.cloudtest.mesher.service.impl;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.unibeta.cloudtest.mesher.config.ConfigurationProxy;
import com.unibeta.cloudtest.mesher.modules.upm.entity.Tenant;
import com.unibeta.cloudtest.mesher.modules.upm.entity.User;
import com.unibeta.cloudtest.mesher.modules.upm.entity.UserLoginHistory;
import com.unibeta.cloudtest.mesher.modules.upm.repo.TenantRepo;
import com.unibeta.cloudtest.mesher.modules.upm.repo.UserLoginHistoryRepo;
import com.unibeta.cloudtest.mesher.modules.upm.repo.UserRepo;
import com.unibeta.cloudtest.mesher.service.EditorService;
import com.unibeta.cloudtest.mesher.service.UpmService;
import com.unibeta.vrules.utils.CommonUtils;

import cn.hutool.crypto.digest.MD5;

@Service
public class UpmServiceImpl implements UpmService {

	private static final String IP_QUERY_SERVICE_126_NET = "http://ip.ws.126.net/ipquery?ip=";
	@Autowired
	TenantRepo tenantRepo;
	@Autowired
	UserRepo userRepo;
	@Autowired
	UserLoginHistoryRepo userLoginHistoryRepo;
	@Autowired
	EditorService editorService;

	@Override
	public User registerUser(User user) throws Exception {
		User u = this.getUserByName(user.getUsername());
		Assert.isTrue(u == null, "username '" + user.getUsername() + "' is exist");

		user.setPassword(MD5.create().digestHex(user.getPassword()));

		File cloudtestHome = new File(ConfigurationProxy.getWorkspace() + "/" + user.getUsername() + "/");
		if (!cloudtestHome.exists() || cloudtestHome.list() == null || cloudtestHome.list().length == 0) {
			editorService.restoreWorkspace(user.getUsername());
		}

		return userRepo.save(user);
	}

	@Override
	public boolean updateUser(User user) throws Exception {

		userRepo.save(user);
		return true;
	}

	@Override
	public User login(User user) throws Exception {
		User u = this.getUserByName(user.getUsername());
		Assert.isTrue(u != null, "username '" + user.getUsername() + "' is not exist");

		String digestHex = MD5.create().digestHex(user.getPassword());

		boolean checkPass = u.getPassword().equals(digestHex);
		Assert.isTrue(checkPass, "password is incorrect");

		u.setLastLoginTime(new Date());
		u.setLastLoginIp(user.getLastLoginIp());
		userRepo.save(u);

		user.setId(u.getId());

		CompletableFuture.runAsync(() -> {
			saveLoginData(user);
		});

		u.setDetails(u);
		return u;
	}

	private void saveLoginData(User user) {
		UserLoginHistory userLogin = new UserLoginHistory();
//		try {
//			Response r = OpenAPIHub.call("get", IP_QUERY_SERVICE_126_NET + user.getLastLoginIp(), null, new HashMap());
//
//			if (r != null && r.getStatus() == 200 && r.getBody() != null) {
//				String[] ipInfos = r.getBody().toString().split("localAddress=");
//
//				if (ipInfos.length >= 2) {
//					ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
//					engine.put("localAddress", null);
//					String script = ipInfos[1].replace("\"", "'");
//
//					Object value = engine.eval("localAddress = " + script);
//					Bindings map = (Bindings) engine.get("localAddress");
//
//					userLogin.setCity(map.get("city").toString());
//					userLogin.setProvince(map.get("province").toString());
//					userLogin.setCountry("中国");
//				}
//			} else {
//				userLogin.setCity(r.getMessage());
//				userLogin.setProvince(r.getMessage());
//				userLogin.setCountry(r.getMessage());
//			}
//		} catch (Exception e) {
//
//			userLogin.setCity("未知");
//			userLogin.setProvince("未知");
//			userLogin.setCountry("未知");
//
////			e.printStackTrace();
//		}

		userLogin.setUserId(user.getId());
		userLogin.setIp(user.getLastLoginIp());
		userLogin.setUsername(user.getUsername());

		userLoginHistoryRepo.save(userLogin);
	}

	@Override
	public User getUserByName(String name) throws Exception {
		return userRepo.getUserByUsername(name);
	}

	@Override
	public User getUserById(Long id) throws Exception {
		return userRepo.getOne(id);
	}

	@Override
	public List<Tenant> getTenantList(Long userId) throws Exception {
		return tenantRepo.getTenantListByUserId(userId);
	}

	@Override
	public Tenant addTenant(Tenant tenant) throws Exception {
		return tenantRepo.save(tenant);
	}

	@Override
	public Boolean changePassword(String username, String oldPassword, String newPassword, String confirmPassword) throws Exception {
		User user = this.getUserByName(username);

		String digestHex = MD5.create().digestHex(oldPassword);

		boolean checkPass = user.getPassword().equals(digestHex);
		Assert.isTrue(checkPass, "old password is incorrect");
		Assert.isTrue(!CommonUtils.isNullOrEmpty(newPassword), "new plassword is mandatory.");
		Assert.isTrue(!CommonUtils.isNullOrEmpty(confirmPassword), "confirmed plassword is mandatory.");

		Assert.isTrue(newPassword.equals(confirmPassword),
				"twice password inputs are inconsistent, please double check.");
		
		user.setPassword(MD5.create().digestHex(newPassword));
		
		this.updateUser(user);

		return false;
	}

}
