package com.unibeta.cloudtest.mesher.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.unibeta.cloudtest.mesher.dto.CloudTest;
import com.unibeta.cloudtest.mesher.dto.TreeModel;

public interface EditorService {

	public List<TreeModel> getFileTree(String rootPath) throws Exception;

	public void save(String content, String filePath) throws Exception;

	public CloudTest load(String filePath) throws Exception;

	public CloudTest run(CloudTest cloudtest) throws Exception;

	public void newFile(String filePath) throws Exception;

	public String importFile(String basePath, MultipartFile file) throws Exception;

	public String exportFile(String basePath) throws Exception;

	public void newFolder(String filePath) throws Exception;

	public String duplicateFile(String filePath) throws Exception;

	public String renameFile(String filePath, String newName) throws Exception;

	public void deleteFile(String filePath) throws Exception;

	public void restoreWorkspace(String username) throws Exception;
	
	public void clearCache() throws Exception;

}
