package com.unibeta.cloudtest.mesher.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.unibeta.cloudtest.mesher.dto.R;

@RestController
@RequestMapping("/restful/webproxy")
public class WebProxyController {

	@GetMapping(name = "request")
	public R request(@RequestParam(name = "url") String url) {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Access-Control-Allow-Origin", "*");
		
		try {
			HttpResponse<String> asString = Unirest.get(url).headers(headers ).asString();
			if (asString.getStatus() ==200) {
				return R.ok(asString.getBody());
			}else {
				return R.fail(asString.getBody());
			}
		} catch (UnirestException e) {
			R.fail(e.getMessage());
		}
		
		return R.ok();
	}
}
