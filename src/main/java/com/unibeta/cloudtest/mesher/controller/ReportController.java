package com.unibeta.cloudtest.mesher.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unibeta.cloudtest.mesher.auth.annotation.Authorization;
import com.unibeta.cloudtest.mesher.auth.context.AuthContextHolder;
import com.unibeta.cloudtest.mesher.dto.R;
import com.unibeta.cloudtest.mesher.dto.Report;
import com.unibeta.cloudtest.mesher.service.ReportService;

@RestController
@RequestMapping("/report")
@Authorization
public class ReportController {

	@Autowired
	ReportService reportService;

	@GetMapping("/getReport")
	public R<Report> getReport() {
		return R.ok(reportService.getReport(AuthContextHolder.getContext().getCurrentUserDetails().getUsername()));
	}
}
