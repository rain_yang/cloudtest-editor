package com.unibeta.cloudtest.mesher.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unibeta.cloudtest.mesher.auth.annotation.Authorization;
import com.unibeta.cloudtest.mesher.auth.context.AuthContextHolder;
import com.unibeta.cloudtest.mesher.config.ConfigurationProxy;
import com.unibeta.cloudtest.mesher.dto.R;
import com.unibeta.cloudtest.mesher.modules.schedule.entity.Job;
import com.unibeta.cloudtest.mesher.modules.schedule.entity.JobRunHistory;
import com.unibeta.cloudtest.mesher.modules.upm.entity.User;
import com.unibeta.cloudtest.mesher.service.ScheduleService;
import com.unibeta.cloudtest.mesher.service.UpmService;

import lombok.extern.slf4j.Slf4j;

@RestController()
@RequestMapping("/schedule")
@Authorization
@Slf4j
public class ScheduleController {

	@Autowired
	ScheduleService scheduleService;

	@Autowired
	UpmService upmService;

	@GetMapping("/getAllJobs")
	public R<List<Job>> getAllJobs() {
		List<Job> jobs;
		try {
			User user = upmService.getUserByName(AuthContextHolder.getContext().getCurrentUserDetails().getUsername());
			jobs = scheduleService.getJobsByUserId(user.getId());
			return R.ok(jobs);
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}
	}

	@GetMapping("/getJob/{jobId}")
	public R<Job> getJob(@PathVariable(name = "jobId") Long jobId) {
		try {
			Job job = scheduleService.getJob(jobId);
			return R.ok(job);
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}
	}

	@PostMapping("/createJob")
	public R<Job> createJob(@RequestBody Job job) {

		try {
			Assert.isTrue(job != null, "job is null");
			if (job.getUserId() == null) {
				Long userId = ((User) AuthContextHolder.getContext().getCurrentUserDetails().getDetails()).getId();
				job.setUserId(userId);
				job.setHome(ConfigurationProxy.getCLOUDTEST_HOME());
			}
			Job createJob = scheduleService.createJob(job);
			if (createJob.getStatus()) {
				scheduleService.startJob(createJob.getId());
			}
			return R.ok(createJob);
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}
	}

	@PostMapping("/removeJob/{jobId}")
	public R removeJob(@PathVariable(name = "jobId") Long jobId) {
		try {
			scheduleService.removeJob(jobId);
			return R.ok();
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}

	}

	@PostMapping("/updateJob")
	public R<Job> updateJob(@RequestBody Job job) {
		try {
			Job updateJob = scheduleService.updateJob(job);

			if (job.getStatus()) {
				scheduleService.stopJob(updateJob.getId());
				scheduleService.startJob(updateJob.getId());
			}

			return R.ok(updateJob);
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}
	}

	@PostMapping("/startJob/{jobId}")
	public R<Boolean> startJob(@PathVariable(name = "jobId") Long jobId) {
		try {
			scheduleService.startJob(jobId);
			Job job = scheduleService.getJob(jobId);
			job.setStatus(true);
			scheduleService.updateJob(job);
			return R.ok();
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}
	}

	@PostMapping("/stopJob/{jobId}")
	public R<Boolean> stopJob(@PathVariable(name = "jobId") Long jobId) {
		try {
			scheduleService.stopJob(jobId);
			Job job = scheduleService.getJob(jobId);
			job.setStatus(false);
			scheduleService.updateJob(job);
			return R.ok();
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}
	}

	@PostMapping("/saveJobRunHistory")
	public R<JobRunHistory> saveJobRunHistory(@RequestBody JobRunHistory jobRunHistory) {
		try {
			return R.ok(scheduleService.saveJobRunHistory(jobRunHistory));
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}
	}

	@GetMapping("/getJobRunHistoryList")
	public R<List<JobRunHistory>> getJobRunHistoryList(@PathVariable(name = "jobId") Long jobId) {
		try {
			return R.ok(scheduleService.getJobRunHistoryList(jobId));
		} catch (Exception e) {
			
			log.debug(e.getMessage(),e);
			return R.fail(e.getMessage());
		}
	}

}
