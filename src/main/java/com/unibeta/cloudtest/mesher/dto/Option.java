package com.unibeta.cloudtest.mesher.dto;

import lombok.Data;

@Data
public class Option {

	String value;
	String label;
}
