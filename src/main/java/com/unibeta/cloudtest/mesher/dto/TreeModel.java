package com.unibeta.cloudtest.mesher.dto;

import java.util.List;

import lombok.Data;

@Data
public class TreeModel {
	
	String id;
	String label;
	boolean isLeaf = false;
	String content;
	List<TreeModel> children;

}
