package com.unibeta.cloudtest.mesher.dto;

import com.unibeta.cloudtest.mesher.auth.bean.AuthUserDetails;
import com.unibeta.cloudtest.mesher.modules.upm.entity.User;

import lombok.Data;

@Data
public class AuthRequest extends AuthUserDetails<User>{
}
