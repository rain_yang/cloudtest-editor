package com.unibeta.cloudtest.mesher.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Report {
	List<Option> reportList = new ArrayList<Option>();
	String reportBaseUrl;
}
