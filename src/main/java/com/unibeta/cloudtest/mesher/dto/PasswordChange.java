package com.unibeta.cloudtest.mesher.dto;

import lombok.Data;

@Data
public class PasswordChange {
	String username;
	String oldPassword;
	String newPassword;
	String confirmPassword;

}
