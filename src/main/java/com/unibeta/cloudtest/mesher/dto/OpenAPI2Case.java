package com.unibeta.cloudtest.mesher.dto;

import lombok.Data;

@Data
public class OpenAPI2Case {
	String casePath;
	String tagMatchers;
	String apiDocs;
}
