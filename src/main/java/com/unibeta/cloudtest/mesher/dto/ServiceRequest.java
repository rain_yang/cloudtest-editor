package com.unibeta.cloudtest.mesher.dto;

import java.util.Map;

import lombok.Data;

@Data
public class ServiceRequest {

	String services;
	Map<String,Object> data;
}
