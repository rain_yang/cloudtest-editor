package com.unibeta.cloudtest.mesher.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class CloudTest {
	String caseUri;
	String content;
	boolean report;
	List<Option> caseList = new ArrayList<Option>();
	String[] cases;
	String result;
	String reportUrl;
	boolean runnable;
	String endpoint;
}
