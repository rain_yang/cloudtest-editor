package com.unibeta.cloudtest.mesher.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class AuthResponse {

	boolean success;
	String message;
	String token;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
	Date expiresAt;
	boolean refreshed;
}
