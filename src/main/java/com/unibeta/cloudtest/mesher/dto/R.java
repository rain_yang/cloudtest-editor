package com.unibeta.cloudtest.mesher.dto;

import java.util.Date;

import lombok.Data;

@Data
public class R<T> {
	
	Boolean status;
	Double took;
	String code;
	String message;
	T data;
	Date timestamp = new Date();

	public static R ok() {
		return R.ok(null);
	}

	public static R ok(Object data) {
		return R.ok(data, null);
	}

	public static R ok(Object data, Double took) {
		R r = new R();

		r.setStatus(true);
		r.setData(data);
		r.setTook(took);

		return r;
	}

	public static R fail(String message) {
		
		return R.fail(null, message);
	}
	
	public static R fail(String code, String message) {
		R r = new R();

		r.setStatus(false);
		r.setCode(code);
		r.setMessage(message);

		return r;
	}

}
