package com.unibeta.cloudtest.mesher.dto;

import lombok.Data;

@Data
public class ServiceResponse {

	boolean status;
	String message;
	Object data;
	double took;
	String timestamp;
}
