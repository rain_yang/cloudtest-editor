package com.unibeta.cloudtest.mesher.modules.upm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.unibeta.cloudtest.mesher.modules.common.AbstractAuditEntity;

import lombok.Data;

@Data
@Proxy(lazy = false)
@Entity
@Table(name = "upm_tenant",indexes= {@Index(name ="idx_upm_tenant_name",columnList="name")})
public class Tenant extends AbstractAuditEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	@Column(nullable = false, unique = false)
	Long userId;
	@Column(nullable = false, unique = false)
	String name;
	@Column(nullable = false, unique = false)
	String workspace;
	String description;
	
}

