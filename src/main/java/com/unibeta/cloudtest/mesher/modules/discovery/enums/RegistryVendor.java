package com.unibeta.cloudtest.mesher.modules.discovery.enums;

public enum RegistryVendor {
	
	NACOS("nacos"),EUREKA("eureka");
	
	final String value;
	
	private RegistryVendor(String value) {
		this.value = value;
	}

}
