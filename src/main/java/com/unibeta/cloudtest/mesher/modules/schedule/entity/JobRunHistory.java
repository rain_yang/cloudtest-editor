package com.unibeta.cloudtest.mesher.modules.schedule.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.unibeta.cloudtest.mesher.modules.common.AbstractAuditEntity;

import lombok.Data;

@Data
@Proxy(lazy = false)
@Entity
@Table(name = "schedule_job_run_history", indexes = {
		@Index(name = "idx_schedule_job_run_history_jobId", columnList = "jobId"),
		@Index(name = "idx_schedule_job_run_history_startTime", columnList = "startTime"),
		@Index(name = "idx_schedule_job_run_history_finishTime", columnList = "finishTime") })

public class JobRunHistory extends AbstractAuditEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	Long jobId;
	Date startTime;
	Date finishTime;
	@Column(columnDefinition = "clob")
	String message;

}
