package com.unibeta.cloudtest.mesher.modules.discovery;

import java.util.List;

import com.unibeta.cloudtest.mesher.modules.discovery.bean.EndpointInstance;

public interface EndpointsDiscovery {

	static final String CLOUDTEST_MESH_REGISTRY_SERVER_HOST = "cloudtest.mesh.registry.server.host";
	
	List<EndpointInstance> list(String serviceName) throws Exception;
	List<EndpointInstance> list(String registryHost,String serviceName) throws Exception;
}
