package com.unibeta.cloudtest.mesher.modules.plugin.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.unibeta.cloudtest.config.ConfigurationProxy;
import com.unibeta.cloudtest.mesher.modules.plugin.ServicePlugin;
import com.unibeta.cloudtest.mesher.util.BeanInvoker;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RuntimeContextServicePluginImpl implements ServicePlugin {

	@Override
	public void plugin(Map<String, Object> context) {
		try {
			Class.forName("com.unibeta.vrulesx.XEngineFactory");

			// Configuration.setVRULES_HOME(ConfigurationProxy.getCloudTestRootPath());
			// Configuration.setThreadlocalEnabled(true);

//			ObjectDigester.fromJava("com.unibeta.vrulesx.config.Configuration.setThreadlocalEnabled(true)");
//			ObjectDigester.fromJava("com.unibeta.vrulesx.config.Configuration.setVRULES_HOME(\""
//					+ ConfigurationProxy.getCloudTestRootPath().replace("\\", "/") + "\")");

			BeanInvoker.invoke("com.unibeta.vrulesx.config.Configuration", "setThreadlocalEnabled",
					new Class[] { boolean.class }, new Object[] { true });
			BeanInvoker.invoke("com.unibeta.vrulesx.config.Configuration", "setVRULES_HOME",
					new Class[] { String.class },
					new Object[] { ConfigurationProxy.getCloudTestRootPath().replace("\\", "/") });

			Object vrules = BeanInvoker.invoke("com.unibeta.vrulesx.XEngineFactory", "getRuleEngine", new Class[] {},
					new Object[] {});
			Object rating = BeanInvoker.invoke("com.unibeta.vrulesx.XEngineFactory", "getRatingEngine", new Class[] {},
					new Object[] {});

			// vrules =
			// ObjectDigester.fromJava("com.unibeta.vrulesx.XEngineFactory.getRuleEngine()");
			// rating =
			// ObjectDigester.fromJava("com.unibeta.vrulesx.XEngineFactory.getRatingEngine()");

			context.put("$vrules$", vrules);
			context.put("$rating$", rating);

		} catch (Exception e) {
			log.warn(e.getMessage());
		}
	}
}
