package com.unibeta.cloudtest.mesher.modules.upm.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unibeta.cloudtest.mesher.modules.upm.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {
	
	public User getUserByUsername(String name);
	
}
