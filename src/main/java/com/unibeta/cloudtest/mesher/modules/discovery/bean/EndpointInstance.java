package com.unibeta.cloudtest.mesher.modules.discovery.bean;

import lombok.Data;

@Data
public class EndpointInstance {
	String serviceName;
	String instanceId;
	String ip;
	Integer port;
	boolean valid;
	boolean enabled;
	boolean healthy;
}
