package com.unibeta.cloudtest.mesher.modules.upm.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unibeta.cloudtest.mesher.modules.upm.entity.Tenant;

@Repository
public interface TenantRepo extends JpaRepository<Tenant, Long> {

	public List<Tenant> getTenantListByUserId(Long userId);
}
