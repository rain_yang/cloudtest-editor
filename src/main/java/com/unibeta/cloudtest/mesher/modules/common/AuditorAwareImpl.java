package com.unibeta.cloudtest.mesher.modules.common;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import com.unibeta.cloudtest.mesher.auth.bean.AuthUserDetails;
import com.unibeta.cloudtest.mesher.auth.context.AuthContextHolder;

@Component
public class AuditorAwareImpl implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
		AuthUserDetails currentUserDetails = AuthContextHolder.getContext().getCurrentUserDetails();

		if (null != currentUserDetails&& null != currentUserDetails.getUsername()) {
			return Optional.of(currentUserDetails.getUsername());
		} else {
			return Optional.of("system");
		}
	}

}
