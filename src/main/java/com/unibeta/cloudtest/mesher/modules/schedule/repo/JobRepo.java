package com.unibeta.cloudtest.mesher.modules.schedule.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unibeta.cloudtest.mesher.modules.schedule.entity.Job;

@Repository
public interface JobRepo extends JpaRepository<Job, Long> {

	public List<Job> getJobsByUserId(Long userId);
}
