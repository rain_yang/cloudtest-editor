package com.unibeta.cloudtest.mesher.modules.upm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.unibeta.cloudtest.mesher.auth.bean.AuthUserDetails;

import lombok.Data;

@Data
@Proxy(lazy = false)
@Entity
@Table(name = "upm_user", indexes= {@Index(name ="idx_upm_user_username",columnList="username")})
public class User extends AuthUserDetails<Object> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	@Column(nullable = false, unique = true)
	String username;
	@Column(nullable = false, unique = false)
	String password;
	String email;
	String company;
	String cellPhone;
	Integer status;
	Date lastLoginTime;
	String lastLoginIp;
}
