package com.unibeta.cloudtest.mesher.modules.plugin;

import java.util.Map;

public interface ServicePlugin {

	public void plugin(Map<String,Object> context);
}
