package com.unibeta.cloudtest.mesher.modules.upm.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unibeta.cloudtest.mesher.modules.upm.entity.UserLoginHistory;
@Repository
public interface UserLoginHistoryRepo extends JpaRepository<UserLoginHistory, Long> {

}
