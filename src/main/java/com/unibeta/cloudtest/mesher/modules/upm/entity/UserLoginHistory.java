package com.unibeta.cloudtest.mesher.modules.upm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.unibeta.cloudtest.mesher.modules.common.AbstractAuditEntity;

import lombok.Data;

@Data
@Proxy(lazy = false)
@Entity
@Table(name = "upm_user_login_history", indexes = {
		@Index(name = "idx_upm_user_login_history_userid", columnList = "userId"),
		@Index(name = "idx_upm_user_login_history_username", columnList = "username")})

public class UserLoginHistory extends AbstractAuditEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	Long userId;
	String username;
	String ip;
	String province;
	String city;
	String country;

}
