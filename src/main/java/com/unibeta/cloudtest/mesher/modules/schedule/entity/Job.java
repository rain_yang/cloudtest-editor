package com.unibeta.cloudtest.mesher.modules.schedule.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.unibeta.cloudtest.mesher.modules.common.AbstractAuditEntity;

import lombok.Data;

@Data
@Proxy(lazy = false)
@Entity
@Table(name = "schedule_job", indexes= {@Index(name ="idx_schedule_job_userId",columnList="userId"),
		@Index(name ="idx_schedule_job_name",columnList="name")})

public class Job extends AbstractAuditEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	Long userId;
	String home;
	@Column(nullable = false, unique = false)
	String name;
	String tasks;
	String mails;
	String cron;
	Boolean status = false;
	@Column(columnDefinition = "clob")
	String message;
}
