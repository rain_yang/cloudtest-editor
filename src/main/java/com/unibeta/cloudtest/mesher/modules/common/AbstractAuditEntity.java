package com.unibeta.cloudtest.mesher.modules.common;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.lang.Nullable;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
@Data
public abstract class AbstractAuditEntity {

	@Column(updatable = true)
	private @Nullable String tenantId;
	
	@CreatedBy
	@Column(updatable = false)
	@JsonIgnore
	@JSONField(serialize = false)
	private @Nullable String createdBy;

	@CreatedDate
	@Column(updatable = false)
	@JsonIgnore
	@JSONField(serialize = false)
	@Temporal(TemporalType.TIMESTAMP) //
	private @Nullable Date createdDate;

	@LastModifiedBy
	@Column(updatable = true)
	@JsonIgnore
	@JSONField(serialize = false)
	private @Nullable String lastModifiedBy;

	@LastModifiedDate
	@Column(updatable = true)
	@JsonIgnore
	@JSONField(serialize = false)
	@Temporal(TemporalType.TIMESTAMP) //
	private @Nullable Date lastModifiedDate;

}
