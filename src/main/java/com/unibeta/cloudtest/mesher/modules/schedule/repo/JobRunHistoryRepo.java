package com.unibeta.cloudtest.mesher.modules.schedule.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unibeta.cloudtest.mesher.modules.schedule.entity.JobRunHistory;

@Repository
public interface JobRunHistoryRepo extends JpaRepository<JobRunHistory, Long> {

	List <JobRunHistory> findAllByJobId(Long jobId);
}
