package com.unibeta.cloudtest.mesher.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.unibeta.cloudtest.mesher.auth.interceptor.AuthenticationInterceptor;
import com.unibeta.vrules.servlets.URLConfiguration;
import com.unibeta.vrules.tools.CommonSyntaxs;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class WebMvcConfiguration extends WebMvcConfigurationSupport {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		String xspringboot = CommonSyntaxs.getXspringboot();
		if (xspringboot != null && xspringboot.endsWith(".jar")) {
			registry.addResourceHandler("/mesher/**/**").addResourceLocations(
					"file:" + URLConfiguration.getSpringbootClasspath() + "/BOOT-INF/classes/index/");
			registry.addResourceHandler("/cloudtest/**/**").addResourceLocations(
					"file:" + URLConfiguration.getSpringbootClasspath() + "/BOOT-INF/classes/index/");
			registry.addResourceHandler("/natimesh/**/**").addResourceLocations(
					"file:" + URLConfiguration.getSpringbootClasspath() + "/BOOT-INF/classes/index/");

		} else {
			registry.addResourceHandler("/mesher/**/**").addResourceLocations("classpath:/index/");
			registry.addResourceHandler("/cloudtest/**/**").addResourceLocations("classpath:/index/");
			registry.addResourceHandler("/natimesh/**/**").addResourceLocations("classpath:/index/");
		}

		String workspace = com.unibeta.cloudtest.mesher.config.ConfigurationProxy.getWorkspace();
		if (workspace == null) {
			log.warn("CloudTest.Mesher 'cloudtet.workspace' has not been configurated.");
		} else {
			if (!(workspace.endsWith("/") || workspace.endsWith("\\"))) {
				workspace += "/";
			}
		}
		registry.addResourceHandler("/workspace/**/**").addResourceLocations("file:" + workspace);

		registry.addResourceHandler("/swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
		registry.addResourceHandler("/swagger**/**").addResourceLocations("classpath:/META-INF/resources/");

//		super.addResourceHandlers(registry);
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOriginPatterns("*").allowCredentials(true)
				.allowedMethods("GET", "POST", "DELETE", "PUT").maxAge(3600);
	}

	@Autowired
	AuthenticationInterceptor authenticationInterceptor;

	/**
	 * Override this method to add Spring MVC interceptors for pre- and
	 * post-processing of controller invocation.
	 * 
	 * @see InterceptorRegistry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(authenticationInterceptor);
		super.addInterceptors(registry);
	}
}
