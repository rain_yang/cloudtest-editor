package com.unibeta.cloudtest.mesher.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.core.type.AnnotatedTypeMetadata;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@org.springframework.context.annotation.Configuration
@EnableSwagger2
@Conditional(SwaggerConfigEnabledCondition.class)
public class SwaggerConfig {
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("com.unibeta.cloudtest")).paths(PathSelectors.any()).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("CloudTest Mesher Service")
				.description("API specification for CloudTest Mesher Service")
				.termsOfServiceUrl("http://www.unibeta.org.cn/").version("0.0.1")
				.contact(new Contact("com.unibeta.cloudtest", "http://www.unibeta.org.cn/", "jimy_xue@163.com"))
				.build();
	}
}

class SwaggerConfigEnabledCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		return !Arrays.asList(context.getEnvironment().getActiveProfiles()).contains("prod");
	}

}
