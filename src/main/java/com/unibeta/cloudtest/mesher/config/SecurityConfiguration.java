package com.unibeta.cloudtest.mesher.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	UserDetailsService tenantDetailsService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(tenantDetailsService);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		/*
		 * enable 'Access-Control-Allow-Origin', allow crossing access between different
		 * domains.
		 */
		http.cors().disable();

		/*
		 * post method will be forbidden after csrf enabled. if enabled, need pass
		 * auth-token from request.
		 */
		http.csrf().disable().headers().frameOptions().disable();
		http.authorizeRequests().antMatchers("/workspace/**/cloudtest/**").denyAll().and().logout().permitAll();
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/index.html", "/mesh/**", "/services/**", "/restful/**", "/swagger**/**",
				"/v2/api-docs", "/workspace/**/reports/**", "/resources/**/**", "/h2/**/**");
	}

}
