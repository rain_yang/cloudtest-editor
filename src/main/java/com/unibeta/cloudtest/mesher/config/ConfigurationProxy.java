package com.unibeta.cloudtest.mesher.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.unibeta.cloudtest.mesher.auth.annotation.Authorization;
import com.unibeta.cloudtest.util.CloudTestUtils;
import com.unibeta.vrules.tools.CommonSyntaxs;
import com.unibeta.vrules.utils.CommonUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConfigurationProxy extends com.unibeta.cloudtest.config.ConfigurationProxy {

	static String apiHost;
	static String workspace;
	private static Boolean authEnabled = null;

	public static String getAPIHost() { 
		if (null != apiHost) {
			return apiHost;
		}

		String pathFromEnvParam = System.getenv("NATIMESH_APIHOST");
		String pathFromProp = System.getProperty("natimesh.apihost");

		if (!CommonUtils.isNullOrEmpty(pathFromEnvParam)) {
			apiHost = pathFromEnvParam;
		} else if (!CommonUtils.isNullOrEmpty(pathFromProp)) {
			apiHost = pathFromProp;
		} else {
			try {
				apiHost = "http://" + InetAddress.getLocalHost().getHostAddress() + ":3927";
			} catch (UnknownHostException e) {
				log.error(e.getMessage(), e);
			}
		}

		String host = apiHost.indexOf(":") > 0 ? apiHost.trim() : apiHost.trim() + ":3927";
		while (host.endsWith("/") || host.endsWith("\\")) {
			host = host.substring(0, host.length() - 1);
		}

		return host;
	}

	public static String getWorkspace() {

		if (null != workspace) {
			return workspace;
		}

		String pathFromEnvParam = System.getenv("CLOUDTEST_WORKSPACE");
		String pathFromProp = System.getProperty("cloudtest.workspace");

		String pathFromEnvParam1 = System.getenv("NATIMESH_WORKSPACE");
		String pathFromProp1 = System.getProperty("natimesh.workspace");

		if (!CommonUtils.isNullOrEmpty(pathFromEnvParam)) {
			workspace = pathFromEnvParam;
		} else if (!CommonUtils.isNullOrEmpty(pathFromProp)) {
			workspace = pathFromProp;
		} else if (!CommonUtils.isNullOrEmpty(pathFromEnvParam1)) {
			workspace = pathFromEnvParam1;
		} else if (!CommonUtils.isNullOrEmpty(pathFromProp1)) {
			workspace = pathFromProp1;
		} else {
			String parent = new File(CommonSyntaxs.getXspringboot()).getParent();
			if (parent == null) {
				workspace = "workspace";
			} else {
				workspace = parent + "/workspace/";
			}

			// throw new RuntimeException("'CLOUDTEST_WORKSPACE' or '-Dcloudtest.workspace='
			// is not defined.");
		}

		CloudTestUtils.checkFile(workspace);

		return workspace;
	}

	public static String getProperty(String key) {
		return getMeshProperties().getProperty(key);
	}

	public static Properties getMeshProperties() {

		Properties props = new Properties();
		try {
			String meshProperteisFilePath = ConfigurationProxy.getCloudTestRootPath() + "/Config/mesh.properties";
			props.load(new FileInputStream(new File(meshProperteisFilePath)));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return props;
	}

	public static Boolean isAuthorizationEnabled() {

		if (null != authEnabled) {
			return authEnabled;
		}

		String env = System.getProperty(Authorization.JVM_PARAM_AUTH_ENABLED_NAME);

		if (!StringUtils.isEmpty(env) && "true".equals(env)) {
			authEnabled = true;
		} else {
			authEnabled = false;
		}

		return authEnabled;
	}

}
