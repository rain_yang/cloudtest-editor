package com.unibeta.cloudtest.mesher.config;

import java.lang.annotation.Annotation;
import java.net.URL;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import com.unibeta.cloudtest.config.plugin.elements.SpringBeanFactoryPlugin;
import com.unibeta.cloudtest.servlet.CloudTestServlet;
import com.unibeta.cloudtest.tool.Tools;
import com.unibeta.vrules.servlets.VRules4jServlet;

@Configuration
@RestController
public class CloudTestBootStrap implements ApplicationContextAware {

	public static final String CLOUDTEST_HOME = "";

	private static ApplicationContext applicationContext = null;

	@Autowired
	private static DataSource DataSource;

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static DataSource getDataSource() {
		return DataSource;
	}

//	@Bean
//	public ServletRegistrationBean cloudTestServletRegistrationBean() {
//
//		ServletRegistrationBean bean = new ServletRegistrationBean(new CloudTestServlet());
//		ConfigurationProxy.setThreadlocalEnabled(true);
//
//		bean.addInitParameter("CLOUDTEST_HOME$PathProvider",
//				"com.unibeta.cloudtest.config.impl.CLOUDTEST_HOME$PathProviderImpl");
//		bean.addInitParameter("ROOT_FOLDER_NAME", "cloudtest");
//		bean.addInitParameter("WEBSERVICE_ENDPOINT_ADDRESS", "/services/cloudtest");
//		bean.addUrlMappings("/services/cloudtest/*");
//		bean.setLoadOnStartup(5);
//
//		return bean;
//	}
//
//	@Bean
//	public ServletRegistrationBean getVRules4jServletServletRegistrationBean() {
//
//		URL url = Thread.currentThread().getContextClassLoader().getResource("");
//		if ("jar".equalsIgnoreCase(url.getProtocol())) {
//			String[] paths = url.getPath().split(".jar!");
//			Tools.setXspringboot(paths[0].split("file:")[1] + ".jar");
//		}
//
//		ServletRegistrationBean bean = new ServletRegistrationBean(new VRules4jServlet());
//
//		bean.addUrlMappings("/vrules4j/*");
//		bean.setLoadOnStartup(1);
//
//		return bean;
//	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	public static class SpringBeanFactoryPluginImpl implements SpringBeanFactoryPlugin {

		private ApplicationContext getApplicationContext() {
			return CloudTestBootStrap.getApplicationContext();
		}

		@Override
		public Object getBean(String name) throws BeansException {

			return getApplicationContext().getBean(name);
		}

		@Override
		public <T> T getBean(String name, Class<T> requiredType) throws BeansException {

			return getApplicationContext().getBean(name, requiredType);
		}

		@Override
		public Object getBean(String name, Object... args) throws BeansException {

			return getApplicationContext().getBean(name, args);
		}

		@Override
		public <T> T getBean(Class<T> requiredType) throws BeansException {

			return getApplicationContext().getBean(requiredType);
		}

		@Override
		public <T> T getBean(Class<T> requiredType, Object... args) throws BeansException {

			return getApplicationContext().getBean(requiredType, args);
		}

		@Override
		public boolean containsBean(String name) {

			return getApplicationContext().containsBean(name);
		}

		@Override
		public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {

			return getApplicationContext().isSingleton(name);
		}

		@Override
		public boolean isPrototype(String name) throws NoSuchBeanDefinitionException {

			return getApplicationContext().isPrototype(name);
		}

		@Override
		public boolean isTypeMatch(String name, Class<?> typeToMatch) throws NoSuchBeanDefinitionException {

			return getApplicationContext().isTypeMatch(name, typeToMatch);
		}

		@Override
		public Class<?> getType(String name) throws NoSuchBeanDefinitionException {

			return getApplicationContext().getType(name);
		}

		@Override
		public String[] getAliases(String name) {

			return getApplicationContext().getAliases(name);
		}

		@Override
		public boolean containsBeanDefinition(String beanName) {

			return getApplicationContext().containsBeanDefinition(beanName);
		}

		@Override
		public int getBeanDefinitionCount() {

			return getApplicationContext().getBeanDefinitionCount();
		}

		@Override
		public String[] getBeanDefinitionNames() {

			return getApplicationContext().getBeanDefinitionNames();
		}

		@Override
		public String[] getBeanNamesForType(Class<?> type) {

			return getApplicationContext().getBeanNamesForType(type);
		}

		@Override
		public String[] getBeanNamesForType(Class<?> type, boolean includeNonSingletons, boolean allowEagerInit) {

			return getApplicationContext().getBeanNamesForType(type, includeNonSingletons, allowEagerInit);
		}

		@Override
		public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {

			return getApplicationContext().getBeansOfType(type);
		}

		@Override
		public <T> Map<String, T> getBeansOfType(Class<T> type, boolean includeNonSingletons, boolean allowEagerInit)
				throws BeansException {

			return getApplicationContext().getBeansOfType(type, includeNonSingletons, allowEagerInit);
		}

		@Override
		public String[] getBeanNamesForAnnotation(Class<? extends Annotation> annotationType) {

			return getApplicationContext().getBeanNamesForAnnotation(annotationType);
		}

		@Override
		public Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> annotationType)
				throws BeansException {

			return getApplicationContext().getBeansWithAnnotation(annotationType);
		}

		@Override
		public <A extends Annotation> A findAnnotationOnBean(String beanName, Class<A> annotationType)
				throws NoSuchBeanDefinitionException {

			return getApplicationContext().findAnnotationOnBean(beanName, annotationType);
		}

		@Override
		public Class<?> getType(String name, boolean allowFactoryBeanInit) throws NoSuchBeanDefinitionException {
			// TODO Auto-generated method stub
			return null;
		}

	}

	public static class XLSUtils extends com.unibeta.cloudtest.mesher.util.XLSUtils{
		//empty
	}
}
