package com.unibeta.cloudtest.mesher.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.unibeta.vrules.tools.CommonSyntaxs;

public class Log {
	
	static Map<String,Logger> loggers = new HashMap<String,Logger>();

	public static Logger getLogger(Class<?> clazz) {
		return LoggerFactory.getLogger(clazz);
	}

	public static Logger getLogger() {

		StackTraceElement[] traces = Thread.currentThread().getStackTrace();
		String className = traces[2].getClassName();
		
		if(loggers.get(className)== null)  {
			loggers.put(className, LoggerFactory.getLogger(className));
		}
		
		return loggers.get(className);
	}

	public static void println(Object... objects) {

		StackTraceElement strack = Thread.currentThread().getStackTrace()[2];
		CommonSyntaxs.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "[println]"
				+ strack.getClassName() + "." + strack.getMethodName() + "()@line[" + strack.getLineNumber() + "]:");

		CommonSyntaxs.println(objects);
	}
}
