package com.unibeta.cloudtest.mesher.util;

import java.io.File;

import org.springframework.util.Assert;

import com.unibeta.cloudtest.mesher.config.ConfigurationProxy;


public class FileUtils {

	public static String getRelativePath(String fullPath) {
		Assert.isTrue(fullPath != null, "given fullPath is null");

		return new File(fullPath).getAbsolutePath()
				.substring(new File(ConfigurationProxy.getCloudTestRootPath()).getAbsolutePath().length() + 1).replace("\\", "/");
	}

	public static String toAbslolutePath(String name) {
		File file = new File(name);

		if (!file.isAbsolute()) {
			file = new File(ConfigurationProxy.getCloudTestRootPath() + File.separator + name);
		}
		return file.getAbsolutePath();
	}
}
