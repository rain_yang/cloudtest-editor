package com.unibeta.cloudtest.mesher.util;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.parser.Feature;

public class JSONUtils {
	
	public static String toJSONString(Object obj) {
		return com.alibaba.fastjson.JSON.toJSONString(obj);
	}

	public static String toJSONString(Object obj, boolean mode) {
		return com.alibaba.fastjson.JSON.toJSONString(obj, mode);
	}

	public static <T> T parseObject(String json, Class<T> clazz) {
		return (T) com.alibaba.fastjson.JSON.parseObject(json, clazz);
	}

	public static <T> T parseObject(String json, TypeReference<T> type, Feature... features) {
		return (T) com.alibaba.fastjson.JSON.parseObject(json, type, features);
	}

	public static JSONObject parseObject(String json) {
		return com.alibaba.fastjson.JSON.parseObject(json);
	}
	public static <T> List<T> parseArray(String json, Class<T> clazz) {
		return com.alibaba.fastjson.JSON.parseArray(json, clazz);
	}
	public static JSONArray parseArray(String json) {
		return com.alibaba.fastjson.JSON.parseArray(json);
	}
}
