package com.unibeta.cloudtest.mesher.util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class BeanInvoker {

	public static Object invoke(String className, String methodName, Class[] parameterTypes, Object[] args)
			throws Exception {

		Class clazz = Class.forName(className);
		
		Method method = clazz.getMethod(methodName, parameterTypes);
		
		if(Modifier.isStatic(method.getModifiers())) {
			return method.invoke(clazz, args);
		}else {
			return method.invoke(clazz.newInstance(), args);
		}
		
	}
}
