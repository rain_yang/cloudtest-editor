package com.unibeta.cloudtest.mesher.auth.context;

import java.util.HashMap;
import java.util.Map;

import com.unibeta.cloudtest.mesher.auth.bean.AuthUserDetails;

import lombok.Data;

@Data
public class AuthContext<T> {
	
	AuthUserDetails<T> currentUserDetails = new AuthUserDetails<T>();
	Map<String,Object> currentSession = new HashMap<String,Object>();
}
