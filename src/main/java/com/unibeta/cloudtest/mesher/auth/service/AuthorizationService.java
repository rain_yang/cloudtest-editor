package com.unibeta.cloudtest.mesher.auth.service;

import com.unibeta.cloudtest.mesher.auth.bean.AuthUserDetails;
import com.unibeta.cloudtest.mesher.modules.upm.entity.User;

public interface AuthorizationService<T> {

	public static final String REDIS_H_SYS_AUTH_TOKENS = "H_SYS_AUTH_TOKENS";

	public static final int TOKEN_EXPIRES_IN_SECONDS_AS_ONE_MINUTE = 60;
	public static final int TOKEN_EXPIRES_IN_SECONDS_AS_ONE_HOUR = 60 * 60;
	public static final int TOKEN_EXPIRES_IN_SECONDS_AS_ONE_DAY = 60 * 60 * 24;
	public static final int TOKEN_EXPIRES_IN_SECONDS_AS_ONE_WEEK = 60 * 60 * 24 * 7;
	public static final int TOKEN_EXPIRES_IN_SECONDS_AS_ONE_MONTH = 60 * 60 * 24 * 30;

	/**
	 * try to login and get valid token by given user details.
	 * 
	 * @param authUser
	 * @return token if login success, otherwise return null.
	 */
	public String login(AuthUserDetails<T> authUser) throws Exception;

	public AuthUserDetails<User> register(AuthUserDetails<T> authUser) throws Exception;

	public boolean logout(AuthUserDetails<T> authUser) throws Exception;

	public boolean changePassword(String username, String oldPassword, String newPassword, String confirmPassword)
			throws Exception;

	public String signToken(AuthUserDetails<T> authUser) throws Exception;

	public boolean authToken(String token) throws Exception;

}
