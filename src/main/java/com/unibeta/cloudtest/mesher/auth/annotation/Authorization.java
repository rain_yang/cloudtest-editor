package com.unibeta.cloudtest.mesher.auth.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorization {

	boolean required() default true;
	
	static final String NAME = "Authorization";
	static final String TOKEN = "_token";
	static final String BEARER = "Bearer ";
	static final String JVM_PARAM_AUTH_ENABLED_NAME = "cloudtest.mesher.auth.enabled";
}
