package com.unibeta.cloudtest.mesher.auth.context;

public class AuthContextHolder {

	static private ThreadLocal<AuthContext> localAuthContext = new ThreadLocal<AuthContext>();

	public static AuthContext getContext() {
		AuthContext authContext = localAuthContext.get();
		return authContext != null ? authContext : new AuthContext();
	}

	public static void setContext(AuthContext context) {
		localAuthContext.set(context);
	}
}
