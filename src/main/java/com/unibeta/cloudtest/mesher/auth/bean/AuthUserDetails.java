package com.unibeta.cloudtest.mesher.auth.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.unibeta.cloudtest.mesher.modules.common.AbstractAuditEntity;

import lombok.Data;

@Data
public class AuthUserDetails<T extends Object> extends AbstractAuditEntity implements UserDetails {

	public final static String ENTITY_TYPE_ONLINE_EDITOR = "1";
	public final static String ENTITY_TYPE_SERVICE_CLIENT = "2";

	/**
	 * 
	 */
	private static final long serialVersionUID = 3270920416661056662L;

	public AuthUserDetails() {

	}

	public AuthUserDetails(T details) {
		BeanUtils.copyProperties(details, this);
		this.details = details;
	}

	String username;
	String password;
	/**
	 * entityType: <br>
	 * '1' for online login, use short token;<br>
	 * others for remote service requester, use long token.
	 */
	String entityType = ENTITY_TYPE_ONLINE_EDITOR;
	String tenantId = "default";
	boolean accountNonExpired = true;
	private boolean accountNonLocked = true;
	private boolean credentialsNonExpired = true;
	private boolean enabled = true;
	Date issueDate;
	Date expireDate;

	/**
	 * Unit is 'Second'.
	 */
	long timeout;
	String token;
	T details;
	Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
	private Date lastLoginTime;

}
