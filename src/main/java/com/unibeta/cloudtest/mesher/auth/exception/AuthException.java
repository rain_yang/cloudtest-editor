package com.unibeta.cloudtest.mesher.auth.exception;

public class AuthException extends Exception {
	

	String code;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public AuthException(String code, String msg) {
		super(msg);
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	

}
