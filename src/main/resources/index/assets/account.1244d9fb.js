var S=Object.defineProperty;var _=Object.getOwnPropertySymbols;var C=Object.prototype.hasOwnProperty,L=Object.prototype.propertyIsEnumerable;var w=(e,n,t)=>n in e?S(e,n,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[n]=t,y=(e,n)=>{for(var t in n||(n={}))C.call(n,t)&&w(e,t,n[t]);if(_)for(var t of _(n))L.call(n,t)&&w(e,t,n[t]);return e};import{i as P,F as j,J as A,Y as B,k as q,H as R,t as V,a as F,j as $,p as H,m as T,q as g,s as E,x as U,y as i,E as Y}from"./vendor.d0002962.js";import{u as D,S as I,b as G,c as J,f as K}from"./index.32ac82cf.js";import{l as N}from"./index.d4f08cc9.js";var h=P({name:"login",setup(){const{t:e}=j(),{proxy:n}=$(),t=D(),u=A(),f=B(),a=q({isShowPassword:!1,user:{username:"test",password:"test",code:"",lastLoginIp:"",lastLoginCity:""},loading:{signIn:!1}}),d=R(()=>K(new Date)),c=async()=>{N.login(a.user).then(s=>{if(!s.status)F.error({message:"Login faild,caused by "+s.message,type:"error"});else{a.loading.signIn=!0;let o=[],l=[],m=["admin"],v=["btn.add","btn.del","btn.edit","btn.link"],x=["test"],k=["btn.add","btn.link"];a.user.username==="admin"?(o=m,l=v):(o=x,l=k);const b={username:a.user.username,token:s.data,photo:a.user.username==="admin"?"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1813762643,1914315241&fm=26&gp=0.jpg":"https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=317673774,2961727727&fm=26&gp=0.jpg",time:new Date().getTime(),authPageList:o,authBtnList:l};I.set("token",s.data),I.set("userInfo",b),t.dispatch("userInfos/setUserInfos",b),t.state.themeConfig.themeConfig.isRequestRoutes?(J(),p()):(G(),p())}})},p=()=>{var s,o,l,m;d.value,((s=u.query)==null?void 0:s.redirect)?f.push({path:(o=u.query)==null?void 0:o.redirect,query:Object.keys((l=u.query)==null?void 0:l.params).length>0?JSON.parse((m=u.query)==null?void 0:m.params):""}):f.push("/"),setTimeout(()=>{a.loading.signIn=!0,e("message.signInText"),n.mittBus.emit("onSignInClick")},300)};return y({currentTime:d,onSignIn:c},V(a))}}),Z=`.login-content-form[data-v-1b3a0e42] {
  margin-top: 20px;
}
.login-content-form .login-content-password[data-v-1b3a0e42] {
  display: inline-block;
  width: 25px;
  cursor: pointer;
}
.login-content-form .login-content-password[data-v-1b3a0e42]:hover {
  color: #909399;
}
.login-content-form .login-content-code[data-v-1b3a0e42] {
  display: flex;
  align-items: center;
  justify-content: space-around;
}
.login-content-form .login-content-code .login-content-code-img[data-v-1b3a0e42] {
  width: 100%;
  height: 40px;
  line-height: 40px;
  background-color: #ffffff;
  border: 1px solid #dcdfe6;
  color: #333;
  font-size: 16px;
  font-weight: 700;
  letter-spacing: 5px;
  text-indent: 5px;
  text-align: center;
  cursor: pointer;
  transition: all ease 0.2s;
  border-radius: 4px;
  user-select: none;
}
.login-content-form .login-content-code .login-content-code-img[data-v-1b3a0e42]:hover {
  border-color: #c0c4cc;
  transition: all ease 0.2s;
}
.login-content-form .login-content-submit[data-v-1b3a0e42] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const r=Y();H("data-v-1b3a0e42");const O=i("span",null,"Login",-1);T();const Q=r((e,n,t,u,f,a)=>{const d=g("el-input"),c=g("el-form-item"),p=g("el-button"),s=g("el-form");return E(),U(s,{class:"login-content-form"},{default:r(()=>[i(c,null,{default:r(()=>[i(d,{type:"text",placeholder:"Input your username","prefix-icon":"el-icon-user",modelValue:e.user.username,"onUpdate:modelValue":n[1]||(n[1]=o=>e.user.username=o),clearable:"",autocomplete:"off"},null,8,["modelValue"])]),_:1}),i(c,null,{default:r(()=>[i(d,{type:e.isShowPassword?"text":"password",placeholder:"Input your password","prefix-icon":"el-icon-lock",modelValue:e.user.password,"onUpdate:modelValue":n[3]||(n[3]=o=>e.user.password=o),autocomplete:"off"},{suffix:r(()=>[i("i",{class:["iconfont el-input__icon login-content-password",e.isShowPassword?"icon-yincangmima":"icon-xianshimima"],onClick:n[2]||(n[2]=o=>e.isShowPassword=!e.isShowPassword)},null,2)]),_:1},8,["type","modelValue"])]),_:1}),i(c,null,{default:r(()=>[i(p,{type:"primary",class:"login-content-submit",round:"",onClick:e.onSignIn,loading:e.loading.signIn},{default:r(()=>[O]),_:1},8,["onClick","loading"])]),_:1})]),_:1})});h.render=Q,h.__scopeId="data-v-1b3a0e42";export{h as default};
