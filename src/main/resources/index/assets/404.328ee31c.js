import{Y as d,p as l,m,q as f,s as c,x as p,y as e,B as n,A as _,E as u}from"./vendor.d0002962.js";var o={name:"404",setup(){const t=d();return{onGoHome:()=>{t.push("/")},router:t}},mounted(){return this.router.push("/")}},F=`.error[data-v-15d57049] {
  height: 100%;
  background-color: white;
  display: flex;
}
.error .error-flex[data-v-15d57049] {
  margin: auto;
  display: flex;
  height: 350px;
  width: 900px;
}
.error .error-flex .left[data-v-15d57049] {
  flex: 1;
  height: 100%;
  align-items: center;
  display: flex;
}
.error .error-flex .left .left-item .left-item-animation[data-v-15d57049] {
  opacity: 0;
  animation-name: error-num;
  animation-duration: 0.5s;
  animation-fill-mode: forwards;
}
.error .error-flex .left .left-item .left-item-num[data-v-15d57049] {
  color: #d6e0f6;
  font-size: 55px;
}
.error .error-flex .left .left-item .left-item-title[data-v-15d57049] {
  font-size: 20px;
  color: #333333;
  margin: 15px 0 5px 0;
  animation-delay: 0.1s;
}
.error .error-flex .left .left-item .left-item-msg[data-v-15d57049] {
  color: #c0bebe;
  font-size: 12px;
  margin-bottom: 30px;
  animation-delay: 0.2s;
}
.error .error-flex .left .left-item .left-item-btn[data-v-15d57049] {
  animation-delay: 0.2s;
}
.error .error-flex .right[data-v-15d57049] {
  flex: 1;
  opacity: 0;
  animation-name: error-img;
  animation-duration: 2s;
  animation-fill-mode: forwards;
}
.error .error-flex .right img[data-v-15d57049] {
  width: 100%;
  height: 100%;
}`;const r=u();l("data-v-15d57049");const h={class:"error"},v={class:"error-flex"},g={class:"left"},x={class:"left-item"},y=e("div",{class:"left-item-animation left-item-num"},"404",-1),b={class:"left-item-animation left-item-title"},w={class:"left-item-animation left-item-msg"},$={class:"left-item-animation left-item-btn"},k=e("div",{class:"right"},[e("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/error/404.png"})],-1);m();const I=r((t,a,B,i,S,z)=>{const s=f("el-button");return c(),p("div",h,[e("div",v,[e("div",g,[e("div",x,[y,e("div",b,n(t.$t("message.notFound.foundTitle")),1),e("div",w,n(t.$t("message.notFound.foundMsg")),1),e("div",$,[e(s,{type:"primary",round:"",onClick:i.onGoHome},{default:r(()=>[_(n(t.$t("message.notFound.foundBtn")),1)]),_:1},8,["onClick"])])])]),k])])});o.render=I,o.__scopeId="data-v-15d57049";export{o as default};
