import{Y as l,p as f,m,q as d,s as c,x as p,y as e,B as n,A as _,E as h}from"./vendor.d0002962.js";import{S as v}from"./index.32ac82cf.js";var a={name:"401",setup(){const t=l();return{onSetAuth:()=>{v.clear(),t.push("/login")}}}},N=`.error[data-v-08f9a688] {
  height: 100%;
  background-color: white;
  display: flex;
}
.error .error-flex[data-v-08f9a688] {
  margin: auto;
  display: flex;
  height: 350px;
  width: 900px;
}
.error .error-flex .left[data-v-08f9a688] {
  flex: 1;
  height: 100%;
  align-items: center;
  display: flex;
}
.error .error-flex .left .left-item .left-item-animation[data-v-08f9a688] {
  opacity: 0;
  animation-name: error-num;
  animation-duration: 0.5s;
  animation-fill-mode: forwards;
}
.error .error-flex .left .left-item .left-item-num[data-v-08f9a688] {
  color: #d6e0f6;
  font-size: 55px;
}
.error .error-flex .left .left-item .left-item-title[data-v-08f9a688] {
  font-size: 20px;
  color: #333333;
  margin: 15px 0 5px 0;
  animation-delay: 0.1s;
}
.error .error-flex .left .left-item .left-item-msg[data-v-08f9a688] {
  color: #c0bebe;
  font-size: 12px;
  margin-bottom: 30px;
  animation-delay: 0.2s;
}
.error .error-flex .left .left-item .left-item-btn[data-v-08f9a688] {
  animation-delay: 0.2s;
}
.error .error-flex .right[data-v-08f9a688] {
  flex: 1;
  opacity: 0;
  animation-name: error-img;
  animation-duration: 2s;
  animation-fill-mode: forwards;
}
.error .error-flex .right img[data-v-08f9a688] {
  width: 100%;
  height: 100%;
}`;const o=h();f("data-v-08f9a688");const g={class:"error"},u={class:"error-flex"},x={class:"left"},y={class:"left-item"},S=e("div",{class:"left-item-animation left-item-num"},"401",-1),b={class:"left-item-animation left-item-title"},w={class:"left-item-animation left-item-msg"},A={class:"left-item-animation left-item-btn"},$=e("div",{class:"right"},[e("img",{src:"https://gitee.com/lyt-top/vue-next-admin-images/raw/master/error/401.png"})],-1);m();const k=o((t,r,I,i,B,z)=>{const s=d("el-button");return c(),p("div",g,[e("div",u,[e("div",x,[e("div",y,[S,e("div",b,n(t.$t("message.noAccess.accessTitle")),1),e("div",w,n(t.$t("message.noAccess.accessMsg")),1),e("div",A,[e(s,{type:"primary",round:"",onClick:i.onSetAuth},{default:o(()=>[_(n(t.$t("message.noAccess.accessBtn")),1)]),_:1},8,["onClick"])])])]),$])])});a.render=k,a.__scopeId="data-v-08f9a688";export{a as default};
