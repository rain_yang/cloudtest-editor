var v=Object.defineProperty;var p=Object.getOwnPropertySymbols;var h=Object.prototype.hasOwnProperty,y=Object.prototype.propertyIsEnumerable;var u=(e,o,n)=>o in e?v(e,o,{enumerable:!0,configurable:!0,writable:!0,value:n}):e[o]=n,_=(e,o)=>{for(var n in o||(o={}))h.call(o,n)&&u(e,n,o[n]);if(p)for(var n of p(o))y.call(o,n)&&u(e,n,o[n]);return e};import{i as V,k as $,t as w,q as a,s as x,x as F,y as t,A as N,B as f,E as k}from"./vendor.d0002962.js";var c=V({name:"login",setup(){const e=$({ruleForm:{userName:"",code:""}});return _({},w(e))}}),j=`.login-content-form[data-v-311a2c3a] {
  margin-top: 20px;
}
.login-content-form[data-v-311a2c3a] .el-input__icon {
  display: inline-block;
}
.login-content-form .login-content-code[data-v-311a2c3a] {
  width: 100%;
  padding: 0;
}
.login-content-form .login-content-submit[data-v-311a2c3a] {
  width: 100%;
  letter-spacing: 2px;
  font-weight: 300;
  margin-top: 15px;
}`;const l=k(),B=l((e,o,n,I,T,C)=>{const d=a("el-input"),r=a("el-form-item"),i=a("el-col"),m=a("el-button"),g=a("el-row"),b=a("el-form");return x(),F(b,{class:"login-content-form"},{default:l(()=>[t(r,null,{default:l(()=>[t(d,{type:"text",placeholder:e.$t("message.mobile.placeholder1"),"prefix-icon":"iconfont icon-dianhua",modelValue:e.ruleForm.userName,"onUpdate:modelValue":o[1]||(o[1]=s=>e.ruleForm.userName=s),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),t(r,null,{default:l(()=>[t(g,{gutter:15},{default:l(()=>[t(i,{span:16},{default:l(()=>[t(d,{type:"text",maxlength:"4",placeholder:e.$t("message.mobile.placeholder2"),"prefix-icon":"el-icon-position",modelValue:e.ruleForm.code,"onUpdate:modelValue":o[2]||(o[2]=s=>e.ruleForm.code=s),clearable:"",autocomplete:"off"},null,8,["placeholder","modelValue"])]),_:1}),t(i,{span:8},{default:l(()=>[t(m,{class:"login-content-code"},{default:l(()=>[N(f(e.$t("message.mobile.codeText")),1)]),_:1})]),_:1})]),_:1})]),_:1}),t(r,null,{default:l(()=>[t(m,{type:"primary",class:"login-content-submit",round:""},{default:l(()=>[t("span",null,f(e.$t("message.mobile.btnText")),1)]),_:1})]),_:1})]),_:1})});c.render=B,c.__scopeId="data-v-311a2c3a";export{c as default};
