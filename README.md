# cloudtest-editor

#### 介绍
cloudtest-editor是基于cloudtest2.0内核为云原生架构系统提供的新一代API分布式测试中台方案,支持租户隔离,实现将OpenAPI json/Postman collecntion json一键转化测试用例并实现用例在线场景编排和运行任务/报告管理,默认集成了OpenAPI Editor可以实现apidoc在线编辑和测试.

该项目前端系统基于[vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)框架开发, 感谢原作者提供的优秀框架.

#### 功能说明

- 为原生云架构下的OpenAPI自动化测试提供完整方案
- 基于CloudTest框架实现的可视化用例管理系统
- 用户/租户级别的运行时数据隔离
- 支持将Swagger2.0以及OpenAPI3.0的接口文档一键式自动生成测试用例和数据模板
- 支持将Postman用例的collection接口文档一键式自动生成测试用例和数据模板
- 集成了Swagger Editor在线编辑接口文档
- 支持对各个用例/场景进行按需编排,灵活定义测试场景
- 支持对测试用例的增删改查操作和实时运行/结果查看
- 支持全场景自动化断言配置管理
- 定时任务配置管理
- 丰富的在线测试报告

#### 安装教程

1.  git clone https://gitee.com/unibeta/cloudtest-editor.git
2.  mvn install
3.  java -jar cloudtest-editor-xxxx.jar
4.  登陆http://localhost:3927/cloudtest/index.html , 注册并登录, 开启cloudtest-editor之旅.

#### 使用说明

1.  JVM启动参数里可以使用-Dcloudtest.workspace='XXXXXX'指定cloudtest-editor工作空间目录, 如果不做配置, 系统默认在cloudtest-editor-XXX.jar启动应用同目录下新建'workspace'目录.

#### 特别感谢
- [vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 技术交流

![QQ群:1012998079](https://images.gitee.com/uploads/images/2021/1128/225548_755f2ae5_5357457.jpeg "discussion@qq.jpg")
